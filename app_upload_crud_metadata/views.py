# Import
import requests
import json
from django.core.files.storage import FileSystemStorage
from django.core import serializers

# PATH
import os.path
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# REST
from rest_framework.response import Response
#for function based views
from rest_framework.decorators import api_view
#for class based views
from rest_framework.views import APIView
#for apiroot reverse
from rest_framework.reverse import reverse
# From Another Service
from api.views import token_validation

# From this Service
from app_upload_crud_metadata.models import Documents

# Create your views here.

@api_view(['POST'])
def upload_file(request):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            # Save Raw File to media
            inputfile = request.FILES.get('inputfile')
            if inputfile == None:
                return Response({'message': 'Your File is empty but token is approved on upload file Function', 'status': 'error'})           
            description = request.POST['description']
            size = inputfile.size/1024
            size_str = "%.2f KB" % size
            newDoc = Documents.objects.create(document=inputfile, name = inputfile.name, description=description, size=size_str)
            path = request.build_absolute_uri('/media/') + str(newDoc.document)
            return Response({'message':'Success Upload File', 'file' : {'id':newDoc.id, 'name':newDoc.name, 'description':newDoc.description , 'size': newDoc.size,'download-URL':path}})
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on upload file Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})

@api_view(['GET'])
def retrieve_all(request):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            # To store files
            files = []
            for i in Documents.objects.all():
                path = request.build_absolute_uri('/media/') + str(i.document)
                tmpFileInfo = {'file-id' : i.id, 'name' : i.name, 'size':i.size, 'description':i.description, 'uploaded-at':i.uploaded_at,'Download-URL':path}
                files.append(tmpFileInfo)
            return Response({'message':'Get All File Detail', 'files' :files})
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on retrieve all Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})

@api_view(['GET'])
def delete_file(request, file_pk):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            delDoc = Documents.objects.get(id=file_pk)
            delDoc.delete()
            return Response({'message':'Success Delete File', 'file_pk' : file_pk})
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on retrieve all Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})

@api_view(['GET'])
def retrieve_file(request, file_pk):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            tmp_doc = Documents.objects.get(id=file_pk)
            path = request.build_absolute_uri('/media/') + str(tmp_doc.document)
            tmp_file = {'file-id' : tmp_doc.id, 'name' : tmp_doc.name, 'size':tmp_doc.size, 'description':tmp_doc.description, 'uploaded-at':tmp_doc.uploaded_at,'Download-URL':path}
            return Response({'message':'Success Get File', 'File' : tmp_file})
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on retrieve file Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})

@api_view(['POST'])
def update_metadata(request):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            file_id = request.POST.get('file-id')
            new_name = request.POST.get('new-name')
            new_description = request.POST.get('new-description')
            old_doc = Documents.objects.get(id=file_id)
            if new_name != None:
                old_doc.name = new_name
            if new_description != None:
                old_doc.description = new_description
            old_doc.save()
            tmp_doc = Documents.objects.get(id=file_id)
            path = request.build_absolute_uri('/media/') + str(tmp_doc.document)
            tmp_file = {'file-id' : tmp_doc.id, 'name' : tmp_doc.name, 'desc':tmp_doc.description, 'size':tmp_doc.size, 'uploaded-at':tmp_doc.uploaded_at,'Download-URL':path}
            return Response({'message':'Success Update File', 'File New Information' : tmp_file})
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on update metadata Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})
