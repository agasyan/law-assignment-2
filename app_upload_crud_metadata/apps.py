from django.apps import AppConfig


class AppUploadCrudMetadataConfig(AppConfig):
    name = 'app_upload_crud_metadata'
