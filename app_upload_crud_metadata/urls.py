from django.urls import path
from django.conf.urls import include, url

from app_upload_crud_metadata import views


#url for app
urlpatterns = [
    #functions
    # Create File and Metadata [POST]
    url(r'^upload/$', views.upload_file, name='compress-file'),

    # Get Metadata with another function (Read) [GET]
    url(r'^get-all/$', views.retrieve_all, name='retrieve-all'),

    # Delete File Metadata and File [GET]
    url(r'^delete/(?P<file_pk>\d+)/$', views.delete_file, name='delete-file'),

    # Get File Metadata each File [GET]
    url(r'^get/(?P<file_pk>\d+)/$', views.retrieve_file, name='retrieve-file'),

    # Update File Metadata each File [POST]
    url(r'^update/$', views.update_metadata, name='update-metadata'),
]