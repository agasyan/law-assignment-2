from django.db import models

# Create your models here.

class Documents(models.Model):
    id = models.AutoField(primary_key=True)
    document = models.FileField(upload_to='uploads/')
    size = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=255, blank=True)