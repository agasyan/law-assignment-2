# Import
import requests
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage

# PATH
import os.path
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# Zip File Library
from pathlib import Path
import zipfile

# REST
from rest_framework.response import Response
#for function based views
from rest_framework.decorators import api_view
#for class based views
from rest_framework.views import APIView
#for apiroot reverse
from rest_framework.reverse import reverse
# From Another Service
from api.views import token_validation

# Create your views here.

@api_view(['POST'])
def compress_file(request):
    approved = token_validation(request.headers['access-token'])
    if approved:
        try:
            # Save Raw File to media
            rawfile = request.FILES.get('inputfile')
            if rawfile == None:
                return Response({'message': 'Your File is empty but token is approved on compress file Function', 'status': 'error'})
            fs = FileSystemStorage()
            filename = fs.save(rawfile.name, rawfile)
            uploaded_file_url = fs.url(filename)
            tmpname = os.path.splitext(filename)[0]

            # Zip File
            zip_obj = zipfile.ZipFile('zip/' + tmpname +'_zipped.zip', 'w')
            zip_obj.write(SITE_ROOT + "/.." + uploaded_file_url, arcname=rawfile.name, compress_type=zipfile.ZIP_DEFLATED)
            zip_obj.close()

            zip_file = open('zip/' + tmpname +'_zipped.zip', 'rb')
            zipfile_name = tmpname + "_zipped.zip"

            response = HttpResponse(zip_file, content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename="%s"' % zipfile_name
            return response
        
        except Exception as e:
            return Response({'message': 'there was an error but token is approved on compress file Function, detail: "' + repr(e) +'"', 'status': 'error'})

    else:
        return Response({'message': 'Provided Token Wrong', 'status': 'error'})
    