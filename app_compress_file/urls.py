from django.urls import path
from django.conf.urls import include, url

from app_compress_file import views


#url for app
urlpatterns = [
    #functions
    url(r'^$', views.compress_file, name='compress-file')
]