from django.apps import AppConfig


class AppCompressFileConfig(AppConfig):
    name = 'app_compress_file'
