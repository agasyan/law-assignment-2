# Import
import requests 

# Constants
OAUTH_CLIENT_ID='phoof5eecuZ9lohpia1aelasie2ook2oiNga4chu'
OAUTH_CLIENT_SECRET='Raiquoh1taedoCo9choo4woovaiqui8quiusha1e'
OAUTH_TOKEN_URL='http://oauth.infralabs.cs.ui.ac.id/oauth/token'
OAUTH_RESOURCE_URL='http://oauth.infralabs.cs.ui.ac.id/oauth/resource'

from django.shortcuts import render

from rest_framework.response import Response

#for function based views
from rest_framework.decorators import api_view

#for class based views
from rest_framework.views import APIView

#for apiroot reverse
from rest_framework.reverse import reverse


@api_view(['GET'])
def get_token(request):
    try:
        user = request.GET.get('username')
        pasw = request.GET.get('password')
        payload = {'username': user, 'password': pasw, 'grant_type': 'password', 'client_id': OAUTH_CLIENT_ID, 'client_secret': OAUTH_CLIENT_SECRET}
        r = requests.post(url = OAUTH_TOKEN_URL, data=payload)
        return Response(r.json())
        
    except Exception as e:
        return Response({'message': 'there was an error on get_token Function, detail: "' + repr(e) +'"', 'status': 'error'})

@api_view(['GET'])
def is_token_valid(request):
    tmp = token_validation(request.headers['access-token'])
    return Response({'message':tmp})

def token_validation(token):
    data_headers = { 'Authorization': 'Bearer ' + token}
    r = requests.get(url = OAUTH_RESOURCE_URL, headers = data_headers)
    try:
      return r.json()["client_id"] == OAUTH_CLIENT_ID
    except Exception as e:
        return False

