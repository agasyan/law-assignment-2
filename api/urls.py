from django.urls import path
from django.conf.urls import include, url

from api import views


#url for app
urlpatterns = [
    #functions
    url(r'^token/get/?$', views.get_token, name='get-token'),
    url(r'^token/valid/$', views.is_token_valid, name='is-token-valid'),
]